#!/bin/bash

NEWLOC=`curl -s https://github.com/zdia/gorilla/wiki | grep -o "http.*gorilla.*\.zip" | cut -f1 -d"\"" | head -1`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi
