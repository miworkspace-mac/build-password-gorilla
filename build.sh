#!/bin/bash -ex

# CONFIG
prefix="PasswordGorilla"
suffix=""
munki_package_name="Password_Gorilla"
display_name="Password Gorilla"
category="Utilities"
description="Password Gorilla – a cross-platform password manager. The Password Gorilla helps you manage your logins. It stores all your user names and passwords, along with login information and other notes, in a securely encrypted file. A single “master password” is used to protect the file. This way, you only need to remember the single master password, instead of the many logins that you use."
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.zip -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

# Extracts all files
unzip app.zip

# make DMG from the inner prefpane
mkdir app_tmp
mv "Password Gorilla.app" app_tmp
hdiutil create -srcfolder app_tmp -format UDZO -o app.dmg

# Build pkginfo
/usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg > app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.9.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" description -string "${description}"
defaults write "${plist}" category -string "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
